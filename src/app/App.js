import React, { Component } from 'react';

class App extends Component {
    
    constructor() {
        super();
        this.state = {
            title: '',
            description: '',
            tasks: [],
            _id: ''
        }
        this.addTask = this.addTask.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        });
    }

    addTask(e) {
        e.preventDefault();
        if(this.state._id) {
            fetch(`/api/tasks/${this.state._id}`, {
                method: 'PUT',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                M.toast({html: 'Task updated!'});
                this.setState({
                    title: '',
                    description: '',
                    _id: ''
                });
                this.fetchTasks();
            });
        } else {
            fetch('/api/tasks', {
                method: 'POST',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(data => {
                M.toast({html: 'Task saved!'});
                this.setState({title: '', description: ''});
                this.fetchTasks();
            })
            .catch(err => console.error(err));
        }
    }

    componentDidMount() {
        this.fetchTasks();
    }

    fetchTasks() {
        fetch('/api/tasks')
        .then(res => res.json())
        .then(data => {       
            this.setState({tasks: data});
            // console.log(this.state.tasks);
        });
    };

    deleteTask(id) {
        if(confirm('Are you sure you want delete it?')) {
            fetch(`/api/tasks/${id}`, {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                M.toast({html: 'Task deleted!'});
                this.fetchTasks();
            })
            .catch(err => console.error(err));  
        }
    }

    updateTask(id) {
        fetch(`/api/tasks/${id}`)
        .then(res => res.json())
        .then(data => {
            this.setState({
                title: data.title,
                description: data.description,
                _id: data._id
            })
        });
    }

    render() {
        return (
            <div>
                {/* NAVIGATION */}
                <nav>
                    <div className="nav-wrapper light-blue darken-4">
                        <div className="container">
                            <a className="brand-logo" href="/">MERN Stack</a>
                        </div>
                    </div>
                </nav>

                <div className="container">
                    <div className="row">
                        <div className="col s5">
                            <div className="card">
                                <div className="card-content">
                                    
                                    <form onSubmit={this.addTask}>

                                        <div className="row">
                                            <div className="input-field col s12">
                                                <input onChange={this.handleChange} type="text" name="title" placeholder="Title" value={this.state.title} autoFocus/>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="input-field col s12">
                                                <textarea onChange={this.handleChange} className="materialize-textarea" 
                                                    rows="2" col="10" name="description" placeholder="Description" 
                                                    value={this.state.description}>
                                                </textarea>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <button type="submit" className = "btn waves-effect waves-teal col s12">
                                                <i className = "material-icons left">add</i>Save
                                            </button>
                                        </div>
                                        
                                    </form>
                                
                                </div>
                            </div>
                        </div> 

                        <div className="col s7">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Description</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.tasks.map(task => {
                                            return (
                                                <tr key={task._id}>
                                                    <td>{task.title}</td>
                                                    <td>{task.description}</td>
                                                
                                                    <td>
                                                        <button onClick={() => this.updateTask(task._id)} className="btn light-blue darken-4" style={{margin: '4px'}}>
                                                            <i className="material-icons">edit</i>
                                                        </button>

                                                        <button onClick={() => this.deleteTask(task._id)} className="btn light-blue darken-4">
                                                            <i className="material-icons">delete</i> 
                                                        </button>
                                                    </td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                
            </div>
        )
    };
};

export default App;