const express = require('express');
const path = require('path');
const morgan = require('morgan');
const router = require('../routes/tasks.routes');
require('dotenv').config();
require('../database');


const config = app => {

    // settings
    app.set('port', process.env.PORT || 3000);
    
    // middlewares
    app.use(morgan('dev'));
    app.use(express.json());

    // static
    app.use(express.static(path.join(__dirname, '../public')));

    // routes
    app.use('/api/tasks', router);

    return app;

};

module.exports = config;