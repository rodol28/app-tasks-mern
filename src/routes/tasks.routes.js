const router = require('express').Router();
const ctrl = require('../controllers/tasks.ctrl');

router.get('/', ctrl.getAllTask);
router.get('/:id', ctrl.getOneTask);
router.post('/', ctrl.saveTask);
router.put('/:id', ctrl.updateTask);
router.delete('/:id', ctrl.deleteTask);

module.exports = router;
