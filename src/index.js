const express = require('express');
const config = require('./config/config');

const app = config(express());

app.listen(app.get('port'), () => {
    console.log(`Server listenig on port ${app.get('port')}`);
});