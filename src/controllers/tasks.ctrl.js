const Task = require('../models/tasks');
const control = {};

// get all tasks
control.getAllTask = async (req, res) => {

    try {
        const tasks = await Task.find();
        res.json(tasks);
    } catch (err) {
        console.error(err);
    }

};

// get one task
control.getOneTask = async (req, res) => {
    const task = await Task.findById(req.params.id);
    res.json(task);
};

// save task
control.saveTask = async (req, res) => {
    const { title, description } = req.body;
    
    const newTask = new Task({
        title,
        description
    });

    await newTask.save();
    res.json({
        status: 'Task saved!'
    });
};

// update task
control.updateTask = async (req, res) => {
    const { title, description } = req.body;
    const newTask = {
        title,
        description
    };

    await Task.findByIdAndUpdate(req.params.id, newTask);

    res.json({
        status: 'Task updated!'
    });
};

// delete task
control.deleteTask = async (req, res) => {
    await Task.findByIdAndDelete(req.params.id);
    res.json({status: 'Task deleted!'});
};

module.exports = control;