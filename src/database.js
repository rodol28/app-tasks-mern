const mongoose = require('mongoose');

mongoose.connect(`${process.env.DATABASE_URL}/${process.env.DATABASE}`, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true
    })
    .then(db => console.log('db is connected'))
    .catch(err => console.error(err));